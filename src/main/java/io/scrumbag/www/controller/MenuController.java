package io.scrumbag.www.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MenuController {

    @RequestMapping("last_week")
    public String lastweekOverview(Model model) {
        model.addAttribute("title", "Last week page");
        return "last_week";
    }

    @RequestMapping("progression_charts")
    public String chartsOverview(Model model) {
        model.addAttribute("title", "Progession page");
        return "progression";
    }

    @RequestMapping("calendar")
    public String calendar(Model model) {
        model.addAttribute("title", "Calendar page");
        return "calendar";
    }

    @RequestMapping("settings")
    public String settings(Model model) {
        model.addAttribute("title", "Settings page");
        return "settings";
    }
}
