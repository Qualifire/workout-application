package io.scrumbag.www.controller;

import io.scrumbag.www.domain.Exercise;
import io.scrumbag.www.domain.Workout;
import io.scrumbag.www.domain.WorkoutSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.util.Arrays;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("title", "Home workout page");

        Workout last = getLastWorkout();
        Workout next = getNextWorkout();
        model.addAttribute("lastWorkout", last);
        model.addAttribute("nextWorkout", next);
        return "index";
    }

    private Workout getLastWorkout() {

        //TODO replace with real calls
        Exercise benchPress = new Exercise(
                "1"
                , "Bench Press"
                , Arrays.asList(
                new WorkoutSet(12, 1, 40l, true),
                new WorkoutSet(10, 1, 50l, true),
                new WorkoutSet(8, 1, 60l,  false),
                new WorkoutSet(6, 1, 70l,  true)
        )
        );
        Exercise bentOverRow = new Exercise(
                "1"
                , "Bent Over Row"
                , Arrays.asList(
                new WorkoutSet(12, 1, 40l, true),
                new WorkoutSet(10, 1, 50l, false),
                new WorkoutSet(8, 1, 60l, true),
                new WorkoutSet(6, 1, 70l, false)
        )
        );
        Exercise squat = new Exercise(
                "1"
                , "Squat"
                , Arrays.asList(
                new WorkoutSet(12, 1, 40l, true),
                new WorkoutSet(10, 1, 50l, true),
                new WorkoutSet(8, 1, 60l, true),
                new WorkoutSet(6, 1, 70l, true)
        )
        );
        return new Workout(Arrays.asList(squat, benchPress,bentOverRow), "Strength A", null, LocalDate.now().minusDays(2l));
    }

    private Workout getNextWorkout() {

        //TODO replace with real calls
        Exercise benchPress = new Exercise(
                "1"
                , "Overhead Press"
                , Arrays.asList(
                new WorkoutSet(12, 1, 40l, null),
                new WorkoutSet(10, 1, 50l, null),
                new WorkoutSet(8, 1, 60l, null),
                new WorkoutSet(6, 1, 70l, null)
        )
        );
        Exercise bentOverRow = new Exercise(
                "1"
                , "Deadlift"
                , Arrays.asList(
                new WorkoutSet(12, 1, 40l, null),
                new WorkoutSet(10, 1, 50l, null),
                new WorkoutSet(8, 1, 60l, null),
                new WorkoutSet(6, 1, 70l, null)
        )
        );
        Exercise squat = new Exercise(
                "1"
                , "Squat"
                , Arrays.asList(
                new WorkoutSet(12, 1, 40l, null),
                new WorkoutSet(10, 1, 50l, null),
                new WorkoutSet(8, 1, 60l, null),
                new WorkoutSet(6, 1, 70l, null)
        )
        );
        return new Workout(Arrays.asList(squat, benchPress,bentOverRow), "Strength B", null, LocalDate.now());
    }
}
