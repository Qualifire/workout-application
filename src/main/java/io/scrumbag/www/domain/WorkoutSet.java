package io.scrumbag.www.domain;

public class WorkoutSet {

    private Integer reps;
    private Integer sets;
    private Long weight;
    private Boolean achieved;

    public WorkoutSet(Integer reps, Integer sets, Long weight, Boolean achieved) {
        this.reps = reps;
        this.sets = sets;
        this.weight = weight;
        this.achieved = achieved;
    }

    public Integer getReps() {
        return reps;
    }

    public Integer getSets() {
        return sets;
    }

    public Long getWeight() {
        return weight;
    }

    public Boolean getAchieved() {
        return achieved;
    }
}
