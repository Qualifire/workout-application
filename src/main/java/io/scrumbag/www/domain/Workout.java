package io.scrumbag.www.domain;

import java.time.LocalDate;
import java.util.List;

public class Workout {
    private List<Exercise> performedExercises;
    private String workoutType;
    private LocalDate workoutDate;
    private String remarks;

    public Workout(List<Exercise> performedExercises, String type, String remarks, LocalDate workoutDate) {
        this.performedExercises = performedExercises;
        this.workoutType = type;
        this.remarks = remarks;
        this.workoutDate = workoutDate;
    }

    public List<Exercise> getPerformedExercises() {
        return performedExercises;
    }

    public String getRemarks() {
        return remarks;
    }

    public LocalDate getWorkoutDate() {
        return workoutDate;
    }

    public String getWorkoutType() {
        return workoutType;
    }
}
