package io.scrumbag.www.domain;

import java.util.List;

public class Exercise {
    private String id;
    private String name;
    private List<WorkoutSet> workoutSet;

    public Exercise(String id, String name, List<WorkoutSet> workoutSet) {
        this.id = id;
        this.name = name;
        this.workoutSet = workoutSet;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<WorkoutSet> getWorkoutSet() {
        return workoutSet;
    }
}
